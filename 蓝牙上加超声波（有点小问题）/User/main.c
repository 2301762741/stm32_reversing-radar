#include <string.h>
#include "stm32f10x.h"                  // Device header
#include "System.h"


uint16_t num=0;
uint8_t zu[]={0x09,'A','B'};

char RxData[100];   //数据缓存


 // GPint(GPB,OutOD,P6);												//GPIO初始化
 //	GPIO_SetBits(GPIOC, GPIO_Pin_13);						//将PC13引脚设置为高电平
 // GPIO_ResetBits(GPIOC, GPIO_Pin_13);						//将PC13引脚设置为低电平
 //	GPIO_WriteBit(GPIOC, GPIO_Pin_14,0);
 //	GPIO_Write(GPIOA,0x0001);                     //将A整块寄存器地址设置 （上面引脚选择应为GPIO_Pin_ALL）
int main(void)
{

	//Music();				//无缘蜂鸣器初始化
	OLED_Init();		//初始化OLED屏
	Timer_Init();		//初始化定时器
	HC_SR04_Init();		//初始化超声波测距模块
	HCint();				//初始化蓝牙
	USE_SendString("ok!");
	
	GPint(GPA,OutPP,P4|P5|P6);	//蜂鸣器的初始化
	//GPIO_ResetBits(GA,P4); GPIO_SetBits(GA,P6); 
	GPIO_SetBits(GA,P5);   //蜂鸣器上电高平
	
	OLED_ShowString(1, 1, "Distance:");		//OLED屏输出字符串
	
	while (1)
	{
		int Distance_mm=sonar_mm();			//获取距离测量结果，单位毫米（mm）		
		int Distance_m=Distance_mm/1000;	//转换为米（m）为单位，将整数部分放入Distance_m
		int Distance_m_p=Distance_mm%1000;	//转换为米（m）为单位，将小数部分放入Distance_m_p
		OLED_Clear_Part(2,1,16);			//将OLDE屏第2行清屏
		OLED_ShowNum(2, 1,Distance_m,numlen(Distance_m));	//显示测量结果的整数部分
		OLED_ShowChar(2, 1+numlen(Distance_m), '.');		//显示小数点
		if(Distance_mm==254)
		{
			OLED_Clear_Part(2,1,16);
			OLED_ShowString(4,6,"OUT!!!");
			//USE_SendString("OUT!!!!");
			Distance_mm = 5000;
			Music_FMQ(30,1000);
		
		}
		else if(Distance_mm<100){								//判断是否小于100毫米
			OLED_ShowString(4,4,"BYD,STOP!!");
			//GPIO_ResetBits(GA,P5);  //低电平触发警报
			//Music_FMQ(200,Dis);

			OLED_ShowChar(2, 1+numlen(Distance_m)+1,'0');								//因为单位是米，所以小于10cm时要加0
			OLED_ShowNum(2, 1+numlen(Distance_m)+2,Distance_m_p,numlen(Distance_m_p));	//显示测量结果的小数部分
			OLED_ShowChar(2, 1+numlen(Distance_m)+2+numlen(Distance_m_p), 'm');			//显示单位
			USE_SendNumber(Distance_mm,numlen(Distance_mm));
		}else																			
		{
			OLED_Clear_Part(4,1,16);
			//GPIO_SetBits(GA,P5);
			
			OLED_ShowNum(2, 1+numlen(Distance_m)+1,Distance_m_p,numlen(Distance_m_p));	//显示测量结果的小数部分
			OLED_ShowChar(2, 1+numlen(Distance_m)+1+numlen(Distance_m_p), 'm');			//显示单位
			USE_SendNumber(Distance_mm,numlen(Distance_mm));
			Music_FMQ(30,10000);
		}
		
		OLED_Clear_Part(3,1,16);			//将OLDE屏第3行清屏
		OLED_ShowNum(3, 1,Distance_mm,numlen(Distance_mm));		//显示单位为毫米的距离结果
		OLED_ShowString(3, 1 + numlen(Distance_mm), "mm");
		Delay_ms(300);						//延时300毫秒
		
	}
	
}

//void TIM2_IRQHandler(void)
//{
//	if (TIM_GetITStatus(TIM2, TIM_IT_Update) == SET)
//	{
//		num++;
//		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
//	}
//}


