#include "stm32f10x.h"                  // Device header
#include "System.h"
#include "Delay.h"
#include "OLED.h"
#include "Timer.h"
#include "HCSR04.h"

#define GPint(a,b,c) GPint(a,b,c,GPIO_Speed_50MHz)
char RxData[100];   //数据缓存
int pan = 200;   //初始化报警阀值(mm)

uint64_t numlen(uint64_t num)//计算数字的长度
{
    uint64_t len = 1;        // 初始长度为1
    for(; num > 9; ++len)    // 判断num是否大于9，否则长度+1
        num /= 10;	         // 使用除法进行运算，直到num小于1
    return len;              // 返回长度的值
}

int main(void)
{	
	OLED_Init();		//初始化OLED屏
	Timer_Init();		//初始化定时器
	HC_SR04_Init();		//初始化超声波测距模块
	HCint();          //蓝牙初始化模块  
	USE_SendString("OK!");
	
	GPint(GPA,OutPP,P5);	//蜂鸣器的初始化
	GPIO_SetBits(GA,P5);   //蜂鸣器上电高平
	
	OLED_ShowString(1, 1, "Distance:");		//OLED屏输出字符串
	
	//HC_GetData(RxData);    //获取蓝牙串口数据 

	while (1)
	{
		int Distance_mm=sonar_mm();			//获取距离测量结果，单位毫米（mm）		
		int Distance_m=Distance_mm/1000;	//转换为米（m）为单位，将整数部分放入Distance_m
		int Distance_m_p=Distance_mm%1000;	//转换为米（m）为单位，将小数部分放入Distance_m_p
		OLED_Clear_Part(2,1,16);			//将OLDE屏第2行清屏
		OLED_Clear_Part(3,1,16);			//将OLDE屏第3行清屏
		

//		if (USE_GetRxFlag() == 1)			//检查串口接收数据的标志位
//		{
//			pan = USE_RxData;		//获取串口接收的数据
//			OLED_ShowNum(4, 1,pan,numlen(pan));	//显示测量结果的整数部分
//		}
		
		if(Distance_mm==254)
		{
			OLED_Clear_Part(2,1,16);
			OLED_ShowString(4,8,"OUT!!!");
			USE_SendString("OUT!!!\n");
		}
		else if(Distance_mm < pan){								//判断是否小于100毫米
			OLED_ShowString(4,4,"BYD,STOP!!");
			GPIO_ResetBits(GA,P5);  //低电平触发警报

			//OLDE第2行清屏
			OLED_ShowNum(2, 1,Distance_m,numlen(Distance_m));	//显示测量结果的整数部分
			OLED_ShowChar(2, 1+numlen(Distance_m), '.');		//显示小数点
			OLED_ShowNum(2, 1+numlen(Distance_m)+1,Distance_m_p,numlen(Distance_m_p));	//显示测量结果的小数部分
			OLED_ShowChar(2, 1+numlen(Distance_m)+1+numlen(Distance_m_p), 'm');			//显示单位
			//OLDE第3行清屏
			OLED_ShowNum(3, 1,Distance_mm,numlen(Distance_mm));		//显示单位为毫米的距离结果
			OLED_ShowString(3, 1 + numlen(Distance_mm), "mm");
			//USE发送
			USE_SendNumber(Distance_mm,numlen(Distance_mm));  //蓝牙显示
			USE_SendString("mm\n");
		}
		else																			
		{
			OLED_Clear_Part(4,1,16);
			GPIO_SetBits(GA,P5);
			
			//OLDE第2行清屏
			OLED_ShowNum(2, 1,Distance_m,numlen(Distance_m));	//显示测量结果的整数部分
			OLED_ShowChar(2, 1+numlen(Distance_m), '.');		//显示小数点
			OLED_ShowNum(2, 1+numlen(Distance_m)+1,Distance_m_p,numlen(Distance_m_p));	//显示测量结果的小数部分
			OLED_ShowChar(2, 1+numlen(Distance_m)+1+numlen(Distance_m_p), 'm');			//显示单位
			//OLDE第3行清屏
			OLED_ShowNum(3, 1,Distance_mm,numlen(Distance_mm));		//显示单位为毫米的距离结果
			OLED_ShowString(3, 1 + numlen(Distance_mm), "mm");
			//USE发送
			USE_SendNumber(Distance_mm,numlen(Distance_mm));  //蓝牙显示
			USE_SendString("mm\n");
		}
		
		Delay_ms(500);						//周期延时
	}   
}
