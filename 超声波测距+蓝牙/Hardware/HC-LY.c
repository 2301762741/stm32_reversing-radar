#include "stm32f10x.h"                  // Device header
#include "System.h"
#include "string.h"
#define GPint(a,b,c) GPint(a,b,c,GPIO_Speed_50MHz)
uint8_t HC_RxSTA;   //标志位


void HCint()
{
	USEint();  //串口初始化 
	
	GPint(GPC,OutPP,P15);  //控制灯口初始化
	GPIO_SetBits(GC,P15);//LED1_OFF();
	
}

/**
  * 函    数：发送数据
  * 参    数：string字符串
  * 参    数：... 可变的参数列表
  * 返 回 值：无
  */
void HC_SendString(char *Buf)
{
	USE_Printf(Buf);
}

/**
  * 函    数：接收数据
  * 参    数：存放数据的缓存
  * 参    数：字符数组
  * 返 回 值：无 ，注意接受数组的函数要清除标志位和BUF
  */
void HC_GetData(char *Buf)
{
	uint32_t count = 0, a = 0;
	while (count < 10000)
	{
		if (USE_GetRxFlag() == 1)
		{
			Buf[a] = USE_RxData;
			a ++;
			count = 0;
			HC_RxSTA = 1;        //读到数据后置标志位为1 
		}
		count ++;
	}
	Buf[a]='\0';            //添加结束位，否则数据会重复
}
